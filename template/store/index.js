import Vue from 'vue'
import Vuex from 'vuex'

import { DefaultStore } from '@mastervida/drive'
import modules from './modules'

for(let i in DefaultStore) {
  modules[i] = DefaultStore[i]
}

Vue.use(Vuex)

const createStore = () => {
  return new Vuex.Store({
    modules,
    strict: process.env.NODE_ENV !== 'production'
  })
}

export default createStore