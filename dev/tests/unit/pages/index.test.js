import { shallowMount, config, createLocalVue } from '@vue/test-utils'
import StartPage from '~/pages/index.vue'

const localVue = createLocalVue()
let wrapper

beforeEach(() => {
  jest.resetModules()
  jest.clearAllMocks()
  jest.useFakeTimers()

  wrapper = null
  wrapper = shallowMount(StartPage, {
    localVue
  })
})

test('Startpage: Test something', () => {
  expect(true).toBe(true)
})