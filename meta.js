module.exports = {
  helpers: {
    escape: function(value) {
      return value.replace(/'/g, '&apos;');
    }
  },
  prompts: {
    name: {
      'type': 'string',
      'required': true,
      'message': 'Project name'
    },
    description: {
      'type': 'string',
      'required': false,
      'message': 'Project description',
      'default': 'DRIVE project'
    },
    author: {
      'type': 'string',
      'message': 'Author'
    },
    port: {
      type: "string",
      required: false,
      message: "DEV Port (localhost:<port>)",
      default: '3000'
    },
    type: {
      type: 'list',
      message: 'Choose Application Type',
      choices: [
        {
          name: 'B2B',
          value: 'b2b'
        },
        {
          name: 'B2C',
          value: 'b2c'
        },
        {
          name: 'B2B2C',
          value: 'b2b2c'
        }
      ]
    },
    benum: {
      type: 'string',
      required: false,
      message: 'Enter a BENUM'
    },
    maincolor: {
      type: 'string',
      required: false,
      message: 'Enter your MainColor (hex)',
      default: '#09c'
    }
  },
  completeMessage: '{{#inPlace}}To get started:\n\n  npm install # Or yarn\n  npm run dev{{else}}To get started:\n\n  cd {{destDirName}}\n mv packages.tmp.json package.json\n npm install # Or yarn\n  npm run dev{{/inPlace}}'
};
