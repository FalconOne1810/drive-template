# DRIVE IBE Template

A [DRIVE IBE](https://github.com/falcon0ne/drive-template) Setup-Template without the distraction of a complicated development environment.

## Prerequisites

Make sure to have `node 8.0+` and `npm 5.0+` installed

## Installation

This is a project template for [vue-cli](https://github.com/vuejs/vue-cli).

``` bash
$ vue init falcon0ne/drive-template my-project  
$ cd my-project                     
# prepare project
$ mv package.tmp.json package.json
# install dependencies
$ npm install
```

> Make sure to use a version of vue-cli >= 2.1 (`vue -V`).

## Usage

### Development

``` bash
$ cd dev
# setup dev-enviroment
$ npm install
# serve with hot reloading at localhost:3000
$ npm run dev
```

Go to [http://localhost:3000](http://localhost:3000)
