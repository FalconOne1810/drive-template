const state = {
  some: true,
  mainColor: '{{ maincolor }}'
}

const mutations = {}
const actions = {}

export default {
  state,
  mutations,
  actions
}